<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 09.07.2019
 * Time: 1:05
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester;


use Requester\Entities\{Request, Response};

interface IRequester
{
    /**
     * @return Response
     */
    public function getResponse(): Response;
    /**
     * @return Request
     */
    public function getRequest(): Request;
}