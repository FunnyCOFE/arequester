<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 09.07.2019
 * Time: 14:16
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester\Entities;


class Response
{
    /** @var array $headers
     *  @var array $cookies
     *  @var string $redirect_url
     */
    private array $headers = array(), $cookies = array();
    private string $redirect_url = "";
    /** @var string $type
     *  @var string $response
     */
    private string $type, $response;
    /**
     * @var mixed $formatted_response
     */
    private $formatted_response;

    /**
     * Response constructor.
     * @param string $response
     * @param $formatted_response
     * @param string $type
     * @param array $headers
     * @param array $cookies
     * @param string $redirect_url
     */
    public function __construct(string $response, $formatted_response, string $type, array $headers, array $cookies, string $redirect_url)
    {
        $this->response = $response;
        $this->formatted_response = $formatted_response ?: $response;
        $this->type = $type;
        $this->headers = $headers;
        $this->cookies = $cookies;
        $this->redirect_url = $redirect_url;
    }

    /**
     * Getter which returning formatted response depends of type (JSON, DOM, etc)
     *
     * @return mixed Formatted of response
     */
    public function getResponse()
    {
        return $this->formatted_response;
    }

    /**
     * Getter which returning redirected url
     *
     * @return string
     */
    public function getRedirectedUrl(): string
    {
        return $this->redirect_url;
    }

    /**
     * Getter which returning type of response
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Getter which returning string response
     *
     * @return string
     */
    public function getStringResponse(): string
    {
        return $this->response;
    }

    /**
     * Getter which returning response cookies
     *
     * @return array
     */
    public function getCookies(): array
    {
        return $this->cookies;
    }

    /**
     * Getter which returning response headers
     *
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}