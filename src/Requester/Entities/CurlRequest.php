<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 09.07.2019
 * Time: 0:29
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester\Entities;


class CurlRequest extends Request
{
    /** @var array $options */
    private array $options;

    /**
     * Request constructor.
     *
     * @param string $url
     * @param int $type
     * @param array $data
     * @param string $useragent
     * @param array $headers
     * @param string $referer
     * @param string $cookie
     * @param bool $redirect
     */
    public function __construct(string $url, int $type = self::GET, array $data = array(), string $useragent = self::CHROME, array $headers = array(), string $referer = "", string $cookie = "", bool $redirect = false)
    {
        parent::__construct($url, $type, $data, $useragent, $headers, $referer, $cookie, $redirect);

        switch ($type) {
            case self::POST:
                $this->preparePostOptions();
                break;
            case self::GET:
                $this->prepareGetOptions();
                break;
        }
    }

    /**
     * Method that configure options for get request
     */
    private function prepareGetOptions(): void
    {
        $this->options = [
            CURLOPT_URL => $this->url . '?' . http_build_query($this->data),
            CURLOPT_USERAGENT => $this->useragent,
            CURLOPT_HTTPHEADER => $this->headers,
            CURLOPT_HEADER => 1,
            CURLOPT_VERBOSE => 1,
            CURLOPT_COOKIE => $this->cookie,
            CURLOPT_REFERER => $this->referer,
            CURLOPT_FOLLOWLOCATION => $this->redirect,
            CURLOPT_RETURNTRANSFER => true
        ];
    }

    /**
     * Method that configure options for post request
     */
    private function preparePostOptions(): void
    {
        $this->options = [
            CURLOPT_URL => $this->url,
            CURLOPT_USERAGENT => $this->useragent,
            CURLOPT_HTTPHEADER => $this->headers,
            CURLOPT_HEADER => 1,
            CURLOPT_VERBOSE => 1,
            CURLOPT_POST => true,
            CURLOPT_FOLLOWLOCATION => $this->redirect,
            CURLOPT_POSTFIELDS => $this->data,
            CURLOPT_COOKIE => $this->cookie,
            CURLOPT_REFERER => $this->referer,
            CURLOPT_RETURNTRANSFER => true
        ];
    }

    /**
     * Getter which returning options for request
     *
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}