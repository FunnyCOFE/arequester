<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 30.05.2019
 * Time: 0:16
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester\Entities;

class Request implements IRequest
{
    /**
     * Attribute for post type of request
     */
    const POST = 0;

    /**
     * Attribute for get type of request
     */
    const GET = 1;

    /**
     * Useragent for Chrome
     */
    const CHROME = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36";

    /**
     * Useragent for Firefox
     */
    const FIREFOX = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0";

    /** @var string $url
     *  @var int $type
     *  @var array $data
     *  @var string $useragent
     *  @var array $headers
     *  @var string $referer
     *  @var string $cookie
     *  @var bool $redirect
     */
    protected bool $redirect;
    protected string $url, $useragent, $referer, $cookie;
    protected int $type;
    protected array $data, $headers;

    /**
     * Request constructor.
     *
     * @param string $url
     * @param int $type
     * @param array $data
     * @param string $useragent
     * @param array $headers
     * @param string $referer
     * @param string $cookie
     * @param bool $redirect
     */
    public function __construct(string $url, int $type = self::GET, array $data = array(), string $useragent = self::CHROME, array $headers = array(), string $referer = "", string $cookie = "", bool $redirect = false)
    {
        $this->url = $url;
        $this->type = $type;
        $this->data = $data;
        $this->useragent = $useragent;
        $this->headers = $headers;
        $this->referer = $referer;
        $this->cookie = $cookie;
        $this->redirect = $redirect;
    }

    /**
     * Getter which returning requested url
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Getter which returning requested type
     *
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * Getter which returning requested data
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Getter which returning requested useragent
     *
     * @return string
     */
    public function getUseragent(): string
    {
        return $this->type;
    }

    /**
     * Getter which returning requested headers
     *
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Getter which returning requested referer
     *
     * @return string
     */
    public function getReferrer(): string
    {
        return $this->referer;
    }

    /**
     * Getter which returning requested cookies
     *
     * @return string
     */
    public function getCookie(): string
    {
        return $this->cookie;
    }

    /**
     * Getter which returning requested redirect
     *
     * @return bool
     */
    public function getRedirect(): bool
    {
        return $this->redirect;
    }
}