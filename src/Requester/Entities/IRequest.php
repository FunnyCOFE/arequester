<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 09.07.2019
 * Time: 0:39
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester\Entities;


interface IRequest
{
    /**
     * @return string
     */
    public function getUrl(): string;
    /**
     * @return int
     */
    public function getType(): int;
    /**
     * @return array
     */
    public function getData(): array;
    /**
     * @return string
     */
    public function getUseragent(): string;
    /**
     * @return array
     */
    public function getHeaders(): array;
    /**
     * @return string
     */
    public function getReferrer(): string;
    /**
     * @return string
     */
    public function getCookie(): string;
    /**
     * @return bool
     */
    public function getRedirect(): bool;
}