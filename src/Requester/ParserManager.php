<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 28.06.2019
 * Time: 15:59
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester;


use Requester\Parsers\Parser;

class ParserManager
{
    /**
     * Init class to response type
     *
     * @param string $response
     * @param string $content_type
     * @return Parser
     */
    public function getParserClass(string $response, string $content_type): Parser
    {
        $type = explode('/', $content_type);
        $class_name = $this->getClassName($type[0]);

        if (class_exists($class_name)) {
            return new $class_name($response, $type[1]);
        } else {
            return new Parser($response, $content_type);
        }
    }

    /**
     * Generate class name
     *
     * @param $content_type
     * @return string
     */
    private function getClassName($content_type): string
    {
        $type = explode('/', $content_type)[0];

        return 'Requester\Parsers\\' . ucfirst($type) . 'Parser';
    }
}