<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 09.07.2019
 * Time: 0:55
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester;


use Requester\Entities\{CurlRequest};
use Logs\Logger;
use Exception;

class CurlRequester extends Requester
{
    /** @var false|resource $channel */
    private $channel;

    /**
     * Requester constructor.
     * @param CurlRequest $request
     * @param bool $parsing
     * @param string $response_type
     * @throws Exception If requested failed
     */
    public function __construct(CurlRequest $request, bool $parsing = true, string $response_type = 'none')
    {
        parent::__construct($request, $parsing, $response_type);
    }

    /**
     * Destructor that close curl connection on destroy
     */
    public function __destruct()
    {
        if ($this->channel) {
            curl_close($this->channel);
        }
    }

    /**
     * Execute request method. Return response if success and false if not.
     *
     * @return string|null
     */
    protected function executeRequest(): ?string
    {
        $this->channel = curl_init();
        curl_setopt_array($this->channel, $this->request->getOptions());
        $response = curl_exec($this->channel);

        if ($response === false) {
            Logger::Log("Curl error: " . curl_error($this->channel), Logger::ERROR, "Requester");
            curl_close($this->channel);
            $this->channel = null;
            return null;
        } else {
            Logger::Log("Success request to " . $this->request->getUrl(), Logger::LOG, "Requester");
            return $response;
        }
    }

    /**
     * Prepare info for parent Response creation
     *
     * @param null|string $response
     * @param bool $parsing
     * @param string $response_type
     * @param array $headers
     * @param array $cookies
     * @param string $redirected_url
     */
    protected function initResponse(?string $response, bool $parsing, string $response_type, array $headers, array $cookies, string $redirected_url): void
    {
        if (!$response) {
            parent::initResponse('', false, 'none', array(), array(), '');
            return;
        }

        if ($response_type == 'none') {
            if ($parsing) {
                $response_type = explode(';', curl_getinfo($this->channel, CURLINFO_CONTENT_TYPE))[0];
            }
        }

        $response = $this->splitResponse($response);
        $redirect_url = curl_getinfo($this->channel, CURLINFO_REDIRECT_URL);

        $headers = $this->parseHeadersAndCookies($response[0]);

        parent::initResponse($response[1], $parsing, $response_type, $headers[0], $headers[1], $redirect_url);
    }

    /**
     * Split headers and body parts
     *
     * @param string $response
     * @return array First headers, second body
     */
    private function splitResponse(string $response): array
    {
        $headers_size = curl_getinfo($this->channel, CURLINFO_HEADER_SIZE);

        $headers = substr($response, 0, $headers_size);

        $response = substr($response, $headers_size);
        $headers = explode("\n", $headers);

        return [$headers, $response];
    }

    /**
     * Parse headers to assoc array
     * @param array $headers
     * @return array 0 - headers, 1 - cookies
     */
    private function parseHeadersAndCookies(array $headers): array
    {
        $parsed_headers = array();
        $parsed_cookies = array();

        foreach ($headers as $header) {
            $buff_header = explode(':', $header, 2);

            if (count($buff_header) == 2) {
                if (stripos($buff_header[0], "cookie") !== false) {
                    $parsed_cookies = array_merge($parsed_cookies, $this->formatCookie(trim($buff_header[1])));
                } else {
                    $parsed_headers[$buff_header[0]] = trim($buff_header[1]);
                }
            }
        }

        return [$parsed_headers, $parsed_cookies];
    }

    /**
     * Parse and push new cookie to array of cookies
     *
     * @param string $cookie HTTP code of cookie
     * @return array
     */
    private function formatCookie(string $cookie): array
    {
        $name_with_garbage = explode('=', $cookie, 2);
        $value_with_garbage = explode('; ', $name_with_garbage[1], 2)[0];
        $expires_with_garbage = explode('; ', @explode('expires=', $name_with_garbage[1], 2)[1], 2)[0];
        $max_age_with_garbage = explode('; ', @explode('Max-Age=', $name_with_garbage[1], 2)[1], 2)[0];
        $path_with_garbage = explode('; ', @explode('path=', $name_with_garbage[1], 2)[1], 2)[0];
        $domain_with_garbage = explode('; ', @explode('domain=', $name_with_garbage[1], 2)[1], 2)[0];

        $formatted_cookie[$name_with_garbage[0]] = [
            'http' => $cookie,
            'value' => $value_with_garbage,
            'expires' => $expires_with_garbage,
            'max_age' => $max_age_with_garbage,
            'path' => $path_with_garbage,
            'domain' => $domain_with_garbage,
            'secure' => (stripos($cookie, "secure") === false ? false : true),
            'httpOnly' => (stripos($cookie, "httpOnly") === false ? false : true),
        ];

        return $formatted_cookie;
    }
}