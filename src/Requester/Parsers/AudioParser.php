<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 28.06.2019
 * Time: 15:55
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester\Parsers;


class AudioParser extends Parser
{
    public function __construct(string $response, string $type)
    {
        parent::__construct($response, $type);
    }
}