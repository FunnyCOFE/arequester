<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 28.06.2019
 * Time: 15:56
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester\Parsers;


use DOMDocument;
use Logs\Logger;


class TextParser extends Parser
{
    /**
     * TextParser constructor.
     * @param string $response
     * @param string $type
     *
     * @uses htmlParse()
     * @uses xmlParse()
     */
    public function __construct(string $response, string $type)
    {
        parent::__construct($response, $type);
    }

    /**
     * Unavailable method
     *
     * @return DOMDocument|bool Parsed HTML elements
     */
    protected function htmlParse()
    {
        $dom = new DOMDocument;
        //$dom->loadHTML($this->response, LIBXML_COMPACT);

        if ($dom->validate()) {
            Logger::Log("Success parsing of DOMDocument (HTML)", Logger::LOG, "ResponseParser");
            return $dom;
        } else {
            Logger::Log("Not valid HTML", Logger::ERROR, "ResponseParser");
            return false;
        }
    }

    /**
     * @return DOMDocument|bool Parsed XML elements
     */
    protected function xmlParse()
    {
        $dom = new DOMDocument;
        $dom->loadXML($this->response);

        if ($dom->validate()) {
            Logger::Log("Success parsing of DOMDocument (XML)", Logger::LOG, "ResponseParser");
            return $dom;
        } else {
            Logger::Log("Not valid XML", Logger::ERROR, "ResponseParser");
            return false;
        }
    }
}