<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 05.07.2019
 * Time: 17:12
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester\Parsers;

use Logs\Logger;
use Exception;
use ReflectionClass;
use ReflectionException;

class Parser
{
    /** @var string|null $type
     *  @var string $response
     */
    protected ?string $type;
    protected string $response;
    protected $formatted_response;

    /**
     * Parsers constructor.
     * @param string|null $response
     * @param string|null $type Type of response
     */
    public function __construct(?string $response, ?string $type)
    {
        $this->response = $response;
        $this->type = $type;

        $method = $this->generateMethodName($type);
        $this->formatted_response = $this->$method();
    }

    /**
     * @param string $name
     * @param array $arguments
     * @throws Exception
     */
    public function __call(string $name, array $arguments)
    {
        try {
            $class_name = (new ReflectionClass($this))->getShortName();
            if ($class_name == "Parser") {
                $content_type = $this->type;
            } else {
                $content_type = str_replace("Parser" , '', $class_name) . '/' . $this->type;
            }
            Logger::Log("No parser realization or undefined response type - " . $content_type, Logger::WARNING, "ResponseParser");
            throw new Exception("Invalid parsing type");
        } catch (ReflectionException $e) {
            Logger::Log($e->getMessage(), Logger::ERROR, "ResponseParser");
        }
    }

    /**
     * Generate name of method to exclude incorrect signs
     *
     * @param string $type
     * @return string
     */
    protected function generateMethodName(?string $type): string
    {
        $type = str_replace('-', '_', $type);
        $type = str_replace('+', '_', $type);

        return $type . "Parse";
    }

    /**
     * @return mixed
     */
    public function getFormattedResponse()
    {
        return $this->formatted_response;
    }
}