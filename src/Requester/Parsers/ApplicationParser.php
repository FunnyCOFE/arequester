<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 28.06.2019
 * Time: 15:54
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester\Parsers;

use Logs\Logger;
use DOMDocument;
use Exception;

class ApplicationParser extends Parser
{
    /**
     * ApplicationParser constructor.
     * @param string $response
     * @param string $type
     *
     * @uses jsonParse()
     * @uses xmlParse()
     */
    public function __construct(string $response, string $type)
    {
        parent::__construct($response, $type);


    }

    /**
     * @return array|bool Parsed JSON array
     */
    protected function jsonParse()
    {
        try {
            $json = json_decode($this->response, true, 512, JSON_THROW_ON_ERROR);
            Logger::Log("Success parsing of JSON", Logger::LOG, "ResponseParser");
            return $json;
        } catch (Exception $e) {
            Logger::Log("JSON parsing error - " . $e->getMessage(), Logger::ERROR, "ResponseParser");
            return false;
        }

    }

    /**
     * @return DOMDocument|bool Parsed XML elements
     */
    protected function xmlParse()
    {
        $dom = new DOMDocument;
        $dom->loadXML($this->response);

        if ($dom->validate()) {
            Logger::Log("Success parsing of DOMDocument (XML)", Logger::LOG, "ResponseParser");
            return $dom;
        } else {
            Logger::Log("Not valid XML", Logger::ERROR, "ResponseParser");
            return false;
        }
    }
}