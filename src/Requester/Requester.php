<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 28.06.2019
 * Time: 15:59
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Requester;


use Requester\Entities\{Request, CurlRequest, Response};
use Exception;

abstract class Requester implements IRequester
{
    /** @var CurlRequest|Request $request
     *  @var Response $response
     */
    protected Request $request;
    protected Response $response;

    /**
     * Execute request method. Return response if success and null if not.
     *
     * @return string|null
     */
    abstract protected function executeRequest(): ?string;

    /**
     * Requester constructor.
     * @param Request|CurlRequest $request
     * @param bool $parsing
     * @param string $response_type
     * @throws Exception If requested failed
     */
    public function __construct(Request $request, bool $parsing = true, string $response_type = 'none')
    {
        $this->request = $request;

        $response = $this->executeRequest();

        if ($response !== false) {
            $this->initResponse($response, $parsing, $response_type, array(), array(), '');
        } else throw new Exception("Invalid request, watch Log files");
    }

    /**
     * Format response and init Response class
     *
     * @param string $response
     * @param bool $parsing
     * @param string $response_type
     * @param array $headers
     * @param array $cookies
     * @param string $redirect_url
     */
    protected function initResponse(?string $response, bool $parsing, string $response_type, array $headers, array $cookies, string $redirect_url): void
    {
        $formatted_response = null;
        if ($parsing) {
            $parser_manager = new ParserManager();
            $parser = $parser_manager->getParserClass($response, $response_type);
            $formatted_response = $parser->getFormattedResponse();
        }

        $this->response = new Response($response, $formatted_response, $response_type, $headers, $cookies, $redirect_url);
    }

    /**
     * Getter which returning class of response
     *
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    /**
     * Getter which returning class of request
     *
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }
}