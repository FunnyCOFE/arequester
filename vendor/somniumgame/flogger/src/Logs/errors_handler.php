<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 06.07.2019
 * Time: 14:39
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

require_once "Logger.php";

use Logs\Logger;

error_reporting(-1);

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }
    switch ($errno) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_PARSE:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
            Logger::Log("Fatal error (id:$errno) in $errfile [$errline]: $errstr", Logger::ERROR, "ErrorLog");
            break;
        case E_WARNING:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_USER_WARNING:
        case E_NOTICE:
            Logger::Log("Warning (id:$errno) in $errfile [$errline]: $errstr", Logger::WARNING, "ErrorLog");
            break;
        case E_USER_NOTICE:
        case E_STRICT:
            Logger::Log("Notice (id:$errno) in $errfile [$errline]: $errstr", Logger::LOG, "ErrorLog");
            break;
        default:
            Logger::Log("Unknown error (id:$errno) in $errfile [$errline]: $errstr", Logger::ERROR, "ErrorLog");
    }
    return true;
});

register_shutdown_function(function () {
    $last_error = error_get_last();
    if($last_error) {
        switch ($last_error['type']) {
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_USER_ERROR:
            case E_RECOVERABLE_ERROR:
            case E_CORE_WARNING:
            case E_COMPILE_WARNING:
            case E_PARSE:
                Logger::Log("Fatal error (id:" . $last_error['type'] . ") in " . $last_error['file'] . " [" . $last_error['line'] . "]: " . $last_error['message'], Logger::ERROR, "ErrorLog");
        }
    }
});