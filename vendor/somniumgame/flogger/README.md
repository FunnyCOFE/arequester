Welcome to simple logger from SomniumGame team!

This logger override default error_log. Any fatal error you can see on  screen in debug mode. All types of logs are storing during some time.

<h3>SETUP:</h3>

1) Use command "composer require somniumgame/flogger".
2) Copy .htaccess from vendor/somniumgame/flogger/src to root dir of your project.
3) Edit .htaccess first line. Replace "path_to_project" with your path to vendor dir (example: from "path_to_project" to "home/somniumgame/public_html")
4) Configure setting in vendor/somniumgame/flogger/src/Configs/logs_config.json :
{
  "path_to_logs_dir": "Logs/", - path to store logs file from root dir of project
  "log_types": [
    "error",
    "log",
    "warning"
  ], - you can add custom types of logs, but need to add screening parameter (at the end of config) and to vendor/somniumgame/flogger/src/Logs/Logger.php too. Add to const and to "prepareMessageClass" method.
  "storage_time_in_days": 3, - fully days without current day (3 day + today)
  "debug_mode": true, - only in this mode logs will be screening. Recommend setup to false in production.
  "warning_screening": true, - show warning logs on the screen
  "log_screening": true  - show log logs on the screen
  HERE CAN BE CUSTOM SCREENING
}

5) Use: Logger::Log("Error text", Logger::ERROR, "ErrorLog", true);
Params: 1) text of log 2) log type 3) name of log classification 4) screening (if true - override all config setting and always show log)
